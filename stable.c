#include <stdlib.h>
#include <stdio.h>
#include <conio.h>

char tilemap [8][3] = {
    "  ", // 0; Floor
    "##", // 1; Wall (Vertical)
    "##", // 2; Wall (Horizontal)
    "..", // 3; Exit floor (Vertical)
    "..", // 4; Exit Floor (Horizontal)
    "II", // 5; Collumn (Pillar, if you wish)
    "--", // 6; N/A
    "--"  // 7; N/A
};

char objmap [4][3] = {
    "  ", // 0; Blank space
    "Hj", // 1; Player
    "iJ", // 2; Candle
    "--"  // 3; N/A
};

short int room[9][9] = {
    {3, 1, 3, 3, 3, 3, 3, 1, 3},
    {0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 5, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 1, 0, 0, 0, 1, 1, 0},
    {0, 1, 0, 0, 0, 0, 0, 1, 0},
    {0, 1, 0, 0, 5, 0, 0, 1, 0},
    {0, 1, 0, 0, 0, 0, 0, 1, 0},
    {3, 1, 3, 3, 3, 3, 3, 1, 3}
};

short int object[9][9] = {
    {0, 0, 2, 0, 0, 0, 2, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 2, 0, 0, 0, 2, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0}
};

/*****/

void render(short int target[])
{
    short int row = 0;
    short int col = 0;

    for (row = 0; row < 9; row += 1) //Nested loop that goes through every cell of current room.
    {
        for (col = 0; col < 9; col += 1)
        {
            if ( /* Uncomprehendanbly complex check for whether the cell is illuminated.
                  * Don't even ask me to comment what is going inside this condition. I can barely understand it myself.
                  */ 
            
     ( ( ( (target[1] - row) + (target[0] - col) <= 3 ) 
            &&
            (target[1] - row < 3 && target[0] - col < 3) )

            && /***/

            ( ( (target[1] - row) + (col - target[0]) <= 3 )
            &&
            (target[1] - row < 3 && col - target[0] < 3) )

            && /***/

           ( ( (row - target[1]) + (target[0] - col) <= 3 )
            &&
            (row - target[1] < 3 && target[0] - col < 3) )

            && /***/
    
            ( ( (row - target[1]) + (col - target[0]) <= 3 )
            &&
            (row - target[1] < 3 && col - target[0] < 3) ) )

            ||

            ( /* This check is "slightly" simpler. If check above was for illumination by player, which has a wide radius
               * and requires excluding corners to make it look more round, this check is for illumination by candles on the floor.
               * Candles on the floor simply illuminate adjacent cells in both cardinal and ordinal directions, hence require no complex conditions.
               */

            (object[row + 1][col] == 2) ||
            (object[row - 1][col] == 2) ||
            (object[row][col + 1] == 2) ||
            (object[row][col - 1] == 2) ||

            (object[row + 1][col + 1] == 2) ||
            (object[row - 1][col + 1] == 2) ||
            (object[row + 1][col - 1] == 2) ||
            (object[row - 1][col - 1] == 2) ||

            (object[row][col] == 2) /* It would've been weird if a candle illuminated everything around, but not itself.
                                     * And this is exactly what happened... At least it was the easiest bug to fix.
                                     */

            ) 
            
            ) 

            {
                if (row == target[1] && col == target[0]) //See if current cell is the one player stands on.
                {
                    printf("%s", objmap[1]); //Show player character if it is.
                }

                else if (object[row][col] != 0) //See if current cell has objects.
                {
                    printf("%s", objmap[object[row][col]]); //Show the appropriate object if it does.
                }

                else
                {
                    printf("%s", tilemap[room[row][col]]); //Show the appropriate tile if it doesn't.
                }
            }

            else
            {
                printf("  "); //Not illuminated.
            }
        }
    printf("\n");
    }
}

/***/

void move(char direction, short int target[])
{
    switch(direction)
    {
        case 'w': //Go up.
            switch(target[1] - 1)
            {
                case -1: //Look for room border collisions.
                    if (room[target[1]][target[0]] == 3)
                    {
                        exit(0); /* Tile 3 is a floor tile from which you can proceed into a room above or below.
                                  * Switching rooms, however, is nimpy, and yet we need some way of indicating that this check works.
                                  */
                    }

                    break;

                default: //Look whether cell above is not a wall.
                    if ( (room[target[1] - 1][target[0]] == 0 ||
                          room[target[1] - 1][target[0]] == 3 ||
                          room[target[1] - 1][target[0]] == 4)
                         &&
                         (object[target[1] - 1][target[0]] == 0 ||
                          object[target[1] - 1][target[0]] == 2) )
                        {
                            target[1] -= 1; //And proceed upwards if it isn't.
                        }

                    if (object[target[1]][target[0]] == 2)
                    {
                        object[target[1]][target[0]] = 0; //Pick up the candle in case the player ends up on a cell with one.
                    }

                    break;
            }
            break;

        case 'd': //Go right.
            switch(target[0] + 1)
            {
                case 9: //Look for room border collisions.
                    if (room[target[1]][target[0]] == 4)
                    {
                        exit(0); /* Tile 4 is a floor tile from which you can proceed into a room to the left or right.
                                  * Switching rooms, however, is nimpy, and yet we need some way of indicating that this check works.
                                  */
                    }

                    break;

                default: //Look whether cell to the right is not a wall.
                    if ( (room[target[1]][target[0] + 1] == 0 ||
                          room[target[1]][target[0] + 1] == 3 ||
                          room[target[1]][target[0] + 1] == 4) 
                          &&
                         (object[target[1]][target[0] + 1] == 0 ||
                          object[target[1]][target[0] + 1] == 2) )
                        {
                            target[0] += 1; //And proceed to the right if it isn't.
                        }

                    if (object[target[1]][target[0]] == 2)
                    {
                        object[target[1]][target[0]] = 0; //Pick up the candle in case the player ends up on a cell with one.
                    }

                    break;
            }
            break;


        case 's': //Go down.
            switch(target[1] + 1)
            {
                case 9: //Look for room border collisions.
                    if (room[target[1]][target[0]] == 3)
                    {
                        exit(0); /* Tile 3 is a floor tile from which you can proceed into a room above or below.
                                  * Switching rooms, however, is nimpy, and yet we need some way of indicating that this check works.
                                  */
                    }

                    break;

                default: //Look whether cell below is not a wall.
                    if ( (room[target[1] + 1][target[0]] == 0 ||
                          room[target[1] + 1][target[0]] == 3 ||
                          room[target[1] + 1][target[0]] == 4)
                         &&
                         (object[target[1] + 1][target[0]] == 0 ||
                          object[target[1] + 1][target[0]] == 2) )
                        {
                            target[1] += 1; //And proceed downwards if it isn't.
                        }

                    if (object[target[1]][target[0]] == 2)
                    {
                        object[target[1]][target[0]] = 0; //Pick up the candle in case the player ends up on a cell with one.
                    }

                    break;
            }
            break;

        case 'a': //Go left.
            switch(target[0] - 1)
            {
                case -1: //Look for room border collisions.
                    if (room[target[1]][target[0]] == 4)
                    {
                        exit(0); /* Tile 4 is a floor tile from which you can proceed into a room to the left or right.
                                  * Switching rooms, however, is nimpy, and yet we need some way of indicating that this check works.
                                  */
                    }

                    break;

                default: //Look whether cell to the left is not a wall.
                    if ( (room[target[1]][target[0] - 1] == 0 ||
                          room[target[1]][target[0] - 1] == 3 ||
                          room[target[1]][target[0] - 1] == 4)
                          &&
                          (object[target[1]][target[0] - 1] == 0 ||
                           object[target[1]][target[0] - 1] == 2) )
                        {
                            target[0] -= 1; //And proceed to the left if it isn't.
                        }

                    if (object[target[1]][target[0]] == 2)
                    {
                        object[target[1]][target[0]] = 0; //Pick up the candle in case the player ends up on a cell with one.
                    }

                    break;
            }
            break;

        default: //Not a valid direction.
            break;
    }
}

/***/

/*****/

/*The one and only,*/ main()
{
    char control;
    short int coordinates[2] = {4, 8};

    system("mode 20, 10");

    while ("Cand'le is the best anime girl")
    {
        control = getch();

        if (control == 3)
        {
            exit(0);
        }

        move(control, coordinates);
        render(coordinates);
    }
}